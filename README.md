# Welcome to xarray_log

xarray_log is a python package to import logger data from geophysical mesurement. It is an alternative to WellCAD software.

## Data importation

Using Wellcad you should export your data using `File > Export > Multiple Files...`

## Supported logger

- [x] [QL40-OBI](https://mountsopris.com/ql40-obi-2g-optical-televiewer/)
- [x] [QL40-FWS](https://mountsopris.com/ql40-fws-full-waveform-sonic/)
- [x] [QL40-CAL](https://mountsopris.com/ql40-cal-3-arm-caliper/)
