import pandas as pd
import xarray as xr
import numpy as np
import natsort
import os
import pickle as pk

def convert_rgb_string_to_array(rgb_string):
    # Split the string by dots and convert to integers
    return np.array([int(x) for x in rgb_string.split('.')])

def load_multiple_files(adr,value_err=-999):

    acq_Time='DepthTime.wad' in os.listdir(adr)
    if acq_Time:
        print('Warning: Time acquistion data !')

    ds=xr.Dataset()
    for ll in natsort.natsorted(os.listdir(adr)):
        if ll.split('.')[-1]=="waw" or ll.split('.')[-1]=="wad":
            df=pd.read_csv(adr+ll,skiprows=[1])
            df.replace(value_err, np.nan, inplace=True)
            if not acq_Time:
                ds[df.columns[1]]=xr.DataArray(np.float64(df[df.columns[1]]),dims=[df.columns[0]])
                if df.columns[0] not in list(ds.coords.keys()):
                    ds.coords[df.columns[0]]=df[df.columns[0]]
            else:
                ds[df.columns[1]]=xr.DataArray(np.float64(df[df.columns[1]]),dims=[df.columns[0]])
                ds.coords['Time']=df[df.columns[0]]
        elif ll.split('.')[-1]=="wag":
            print('Loading image, it can be long ...')
            rgb=pd.read_csv(adr+ll,skiprows=[1])
            vec_convert_rgb_string_to_array = np.vectorize(convert_rgb_string_to_array, signature='()->(n)')
            rgb_array = vec_convert_rgb_string_to_array(rgb[rgb.columns[1:]])
            
            ds['Image']=xr.DataArray(rgb_array,dims=['Depth_im','Angle','rgb'])
            ds.coords['Depth_im']=rgb[rgb.columns[0]]
            ds.coords['Angle']=np.linspace(0,360,np.shape(rgb_array)[1])
        elif ll.split('.')[-1]=="waf":
            fws=pd.read_csv(adr+ll,skiprows=[1])
            if not acq_Time:
                ds[ll.split('.')[0]]=xr.DataArray(np.float64(fws[fws.columns[1:]]),dims=[fws.columns[0],'time_aqc'])
                ds.coords[fws.columns[0]]=np.float64(fws[fws.columns[0]])
                ds.coords['time_aqc']=np.float64(fws.columns[1:].str.replace(' us', '').astype(float))
            else:
                ds[ll.split('.')[0]]=xr.DataArray(np.float64(fws[fws.columns[1:]]),dims=['Time','time_aqc'])
                ds.coords['Time']=np.float64(fws[fws.columns[0]])
                ds.coords['Time_aqc']=np.float64(fws.columns[1:].str.replace(' us', '').astype(float))

        else:
            print('Cannot open '+ll.split('.')[-1]+' file format !')

    return ds

def load(path):
    """
    Load xarray aita results stored with pickle by xr.aita.save()

    :param path: path to pickle file
    :type path: string
    """

    ds = pk.load(open(path, "rb"))

    return ds

