import xarray as xr
import numpy as np
import matplotlib.pyplot as plt

@xr.register_dataset_accessor("xrwelllog_fws")

class xrwelllog_fws(object):
    '''
    This is a classe to work on well logging data
    '''
    
    def __init__(self, xarray_obj):
        '''
        Constructor for xrlog. 
        
        '''
        self._obj = xarray_obj 
    pass

#######################################
    def average_same_depth(self,nb=100):
        
        ds_FWS=self._obj
        
        select_depth=[]
        mean_profil_RXi=[[],[],[],[]]
        for depth in np.unique(ds_FWS.DepthTime):
            if np.sum(ds_FWS.DepthTime==depth)>=nb:
                select_depth.append(depth)
                id=np.where(ds_FWS.DepthTime==depth)
                id_T=ds_FWS.Time[id]
                ds_sub=ds_FWS.sel(Time=slice(id_T[3],id_T[-3]))
                for i in range(4):
                    mean_profil_RXi[i].append(ds_sub['RX'+str(i+1)+'-1A'].mean('Time').values)
                
        for i in range(4):
            ds_FWS['RX'+str(i+1)+'_mean']=xr.DataArray(np.stack(mean_profil_RXi[i]),dims=['depth_mean','time_aqc'])

        ds_FWS['depth_mean']=np.array(select_depth)

        return ds_FWS
    
    def plot_waf_mean(self,depth,scale=10**-5,ax=None, **kwargs):

        ds_FWS=self._obj

        pos=depth-np.array([0,0.2,0.4,0.6])-0.6
        id=np.where(np.abs(ds_FWS.depth_mean-depth)==np.min(np.abs(ds_FWS.depth_mean-depth)))[0][0]

        if ax is None:
            fig, ax = plt.subplots(nrows=1,ncols=1, **kwargs)

        for i in range(4):
            ax.plot(ds_FWS['time_aqc'],ds_FWS['RX'+str(i+1)+'_mean'][id,:]*scale+pos[i],label='RX'+str(i+1))
            
        ax.set_xlabel('time (ms)')
        ax.set_ylabel('depth (m)')
        ax.invert_yaxis()

        return 

    def plot_waf(self,depth,scale=10**-5,ax=None, **kwargs):
        
        ds_FWS=self._obj

        pos=depth-np.array([0,0.2,0.4,0.6])-0.6
        id=np.where(np.abs(ds_FWS.DepthTime-depth)==np.min(np.abs(ds_FWS.DepthTime-depth)))[0][0]

        if ax is None:
            fig, ax = plt.subplots(nrows=1,ncols=1, **kwargs)

        for i in range(4):
            ax.plot(ds_FWS['time_aqc'],ds_FWS['RX'+str(i+1)+'-1A'][id,:]*scale+pos[i],label='RX'+str(i+1))
            
        ax.set_xlabel('time (ms)')
        ax.set_ylabel('depth (m)')
        ax.invert_yaxis()

        return 


