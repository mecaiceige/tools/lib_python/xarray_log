import matplotlib.pyplot as plt
import xarray as xr

def summary_obi(self,list,fsz=(14,20),**kwarg):
    '''
    Plot summary of the log file
    '''

    colors=['k','r','b','g']

    fig=plt.figure(figsize=fsz)
    gs = fig.add_gridspec(1, 2,  width_ratios=(1, 1))

    ax0 = fig.add_subplot(gs[0, 0])
    ax0.plot(list[0],list[0].Depth,color=colors[0])
    ax0.tick_params(axis='x', color=colors[0])
    ax0.set_xlabel(list[0].name)

    ax0m=[]
    for i in range(len(list)-1):
        ax0m.append(ax0.twiny())
        ax0m[i].spines.top.set_position(("axes", 1.0+i*0.03))
        ax0m[i].plot(list[i+1],list[i+1].Depth,color=colors[i+1])
        ax0m[i].tick_params(axis='x', colors=colors[i+1])
        ax0m[i].set_xlabel(list[i+1].name)
        ax0m[i].xaxis.label.set_color(colors[i+1]) 

    ax0.set_ylim(ax0.get_ylim()[::-1])
    ax0.grid()
    ax1 = fig.add_subplot(gs[0, 1], sharey=ax0)
    self._obj.Image.plot.imshow(yincrease=False,ax=ax1)

    plt.tight_layout(pad=2.0)

    return fig

xr.Dataset.xrwelllog.summary_obi = summary_obi